/**
 * Displays text sent over the serial port (e.g. from the Serial Monitor) on
 * an attached LCD.
 */
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>

//#define DEBUG = 1
//#define SIMULATE_SPEED = 1

// Some useful time memories..
unsigned long curTime = 0;
unsigned long lastTime = 0;
unsigned long deltaTime = 0;

// Define our software serial port for the GPS on pin 10 & 11
SoftwareSerial serConn(10,11);

TinyGPSPlus gps;

// Flow Sensor Definitions
float ticksPerLitre=480;
float boom_length[] = { 1.4, 2, 2.5, 8 };
unsigned int boom_selector = 3;
unsigned int boom_selector_length = 3; // # of entries in boom_legnth array -  1.

unsigned int button_pressed = 0; // Set if button press is detected.
//
unsigned long flow1Ticks=0;
unsigned long flow1TicksLast=0;
float flow1lpm=0;
float flow1rate=0;

// 
unsigned long flow2Ticks=0;
unsigned long flow2TicksLast=0;
float flow2lpm=0;
float flow2rate=0;

float flowLpmBuffer[3] = { 0.0, 0.0, 0.0 };
float speedBuffer[3] = { 0.0, 0.0, 0.0 };
float flowRateTotal=0;
float avgSpeed=0;
float avgLpm=0;

//
unsigned int cycleCounter = 0;
unsigned int currentCycle = 0;

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 19, 3);

int firstPass = 0; // For clearing the LCD Screen when first entering loop()
char buf[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,0x00 }; // used for sprintf buffers
char blanker_speed[] = "     ";
char blanker[] = "        ";

/*
 * Interupt Handlers - Flow 1 & Flow 2
 */
void flow1Int() {
  flow1Ticks++;
}
void flow2Int() {
  flow2Ticks++;
}

/*
 *  Standard system setup
 */
void setup()
{
	lcd.begin(19,3);
	lcd.init();
	lcd.backlight();
  
  
	// Initialize the serial port at a speed of 9600 baud
#ifdef DEBUG
  Serial.begin(115200);
#endif
  serConn.begin(9600);

  lcd.clear();
  lcd.println("Starting...");
#ifdef DEBUG
  Serial.println("Starting...");
#endif

    //Set up ISRs for flow metres
  //
  pinMode(2, INPUT_PULLUP); // For flow sensor 1 - Intterupt driven.
  pinMode(3, INPUT_PULLUP); // For flow sensor 2 - Interrupt driven
  pinMode(6, INPUT_PULLUP); // For Button - polling, no interrupt.
  attachInterrupt(digitalPinToInterrupt(2), flow1Int, FALLING);
  attachInterrupt(digitalPinToInterrupt(3), flow2Int, FALLING);
  delay(1000);
  
  lcd.clear();
  lastTime=millis();
}

void loop()
{
    while(serConn.available()) {
      gps.encode(serConn.read());
    }

    curTime = millis();
    deltaTime = curTime - lastTime;

    if( digitalRead(6) == 0 && deltaTime > 500 ) button_pressed = 1;
    
    if( deltaTime >999 ) {

      if (flow1Ticks < flow1TicksLast) {
        flow1lpm = (flow1Ticks + ( 0xFFFFFFFF - flow1TicksLast)) / ticksPerLitre;
      } else {
        flow1lpm = (flow1Ticks - flow1TicksLast) / ticksPerLitre;
      }

//      flow1lpm = flow1Ticks / ticksPerLitre;

      flow1lpm = flow1lpm / deltaTime * 1000.0 * 60;
      flow1TicksLast = flow1Ticks;
//      flow1Ticks = 0;


      if (flow2Ticks < flow2TicksLast) {
        flow2lpm = (flow2Ticks + ( 0xFFFFFFFF - flow1TicksLast)) / ticksPerLitre;
      } else {
        flow2lpm = (flow2Ticks - flow2TicksLast) / ticksPerLitre;
      }

//      flow2lpm = flow2Ticks / ticksPerLitre;
      
      flow2lpm = flow2lpm / deltaTime * 1000.0 * 60;
      flow2TicksLast = flow2Ticks;
//      flow2Ticks = 0;


      currentCycle = cycleCounter % 3;
      cycleCounter++;

#ifdef DEBUG
      Serial.print("curTime: ");
      Serial.print(curTime);
      Serial.print(" || currentCycle: ");
      Serial.print(currentCycle);
      Serial.print(" || cycleCounter: ");
      Serial.println(cycleCounter);
#endif

      flowLpmBuffer[currentCycle] = flow1lpm + flow2lpm;
#ifdef SIMULATE_SPEED
      speedBuffer[currentCycle] = 5.1;
#else
      speedBuffer[currentCycle] = gps.speed.kmph();
#endif

//      flow1rate = gps.speed.kmph() * 1000.0 / 3.6 / deltaTime * boom_length[boom_selector] ; //area covered in deltaTime
//      flow1rate = flow1lpm / 60 / flow1rate * 10000; // convert lpm to lps for covered area, then scale to 1Ha
//
//      flow2rate = gps.speed.kmph() * 1000.0 / 3.6 / deltaTime * boom_length[boom_selector] ; //area covered in deltaTime
//      flow2rate = flow2lpm / 60 / flow2rate * 10000; // convert lpm to lps for covered area, then scale to 1Ha

//      flowRateTotal = flow1rate + flow2rate;
      
      lastTime = curTime;

      if (button_pressed != 0 ) {
        boom_selector++;
        if (boom_selector > boom_selector_length) boom_selector=0;
        button_pressed = 0;
      }
    
      if (currentCycle == 0 ) {
  
        avgSpeed = ( speedBuffer[0] + speedBuffer[1] + speedBuffer[2] ) / 3;
        avgLpm = ( flowLpmBuffer[0] + flowLpmBuffer[1] + flowLpmBuffer[2] ) / 3;
        flowRateTotal = avgSpeed * 1000.0 / 3.6 / deltaTime * boom_length[boom_selector] ; //area covered in deltaTime
        flowRateTotal = avgLpm / 60 / flowRateTotal * 10000; // convert lpm to lps for covered area, then scale to 1Ha
  
        
        // lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Speed: ");
        lcd.setCursor(7,0);
        lcd.print(blanker_speed);
        lcd.setCursor(7,0);
        dtostrf(avgSpeed, 3, 1, buf);
        lcd.print(buf);
        // Set length of boom
        lcd.setCursor(0,1);
        lcd.print("Boom:");
        lcd.setCursor(6,1);
        lcd.print(blanker_speed);
        lcd.setCursor(6,1);
        dtostrf(boom_length[boom_selector], 3, 1, buf);
        lcd.print(buf);
        lcd.setCursor(12,1);
        lcd.print("Dir:");
        int dir = (int)gps.course.deg();
        lcd.print(dir);
        // Sats
        lcd.setCursor(12,0);
        lcd.print("Sats:");
        lcd.print(blanker);
        lcd.setCursor(17,0);
        dtostrf(gps.satellites.value(), 2, 0, buf);
        lcd.print(buf);
        // LPM
        lcd.setCursor(0,2);
        lcd.print("LPM:");
        lcd.setCursor(12,2);
        lcd.print(blanker);
        lcd.setCursor(12,2);
        dtostrf(avgLpm,3,1,buf);
        lcd.print(buf);
        // Ltrs/Ha
        lcd.setCursor(0,3);
        lcd.print("Litres/Ha:");
        lcd.setCursor(12,3);
        lcd.print(blanker);
    //    lcd.print(blanker);
        lcd.setCursor(12,3);
        dtostrf(flowRateTotal,3,1,buf);
        lcd.print(buf);
  
  #ifdef DEBUG
        Serial.print("Boom Length: ");
        Serial.print(boom_length[boom_selector]);
        Serial.print(" || avgSpeed: ");
        Serial.print(avgSpeed);
        Serial.print(" || Ticks from flow 1: ");
        Serial.print(flow1Ticks);
        Serial.print(" || LPM: ");
        Serial.print(flow1lpm);
        Serial.print(" || Lt/Ha: ");
        Serial.print(flow1rate);
        Serial.print(" || Ticks flow 1: ");
        Serial.print(flow1Ticks);
        Serial.print(" || Ticks flow 2: ");
        Serial.print(flow2Ticks);
        Serial.print(" || avgLPM: ");
        Serial.print(avgLpm);
        Serial.print("  || Lt/Ha: ");
        Serial.println(flowRateTotal);
  #endif
        
    }
  }
}
