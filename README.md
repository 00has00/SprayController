# Basic Spray Controller

By spray controller, we do of course mean, monitor. This project was designed to allow for monitoring of some basic statistics while using a spray unit.

You can monitor Litres/minute and Litres per hectare. While reported as a single number, up to 2 booms can be metered at once.

The GPS is used to get an over-the-ground speed reading, which is used in conjunction with the boom width to provide area per unit of time that feeds into the Ltrs/Ha calculation.

As a bonus, the ground speed is reported on the screen along with a compass heading that can be used to keep on track with, or to work out your return track.

## System Overview
### Schematic
![Schematic](images/Spray_controller_v1.0_bb.jpg)
_Figure 1: Schematic_

### Parts List
 * Arduino Uno
 * 20x4 LCD with i2c interface
 * GY-NEO6Mv2 - GPS module
 * Momentary button - This one is IP65 rated
 * YF-S201 Flow sensor
 * Cable - Used 7core trailer cable to connect head unit to second enclosure with power supply and cabling loom in it.
 * LM2596S ADJ Power supply module DC-DC Step-down module 5V/12V/24V - 12V to 5V step down.
 * Cigarette Lighter Plug - FUSED
 * Cable glands and seals to allow weather proof cable entry to enclosures
 * 2x Enclosuers - Choose the size you think you can pack stuff into, but the clear lid on one of them for mouting the LCD is easy.

### Libraries

 * `TinyGPS++`
 * `LiquidCrystal_I2C`
 * `Wire.h` - Native include
 * `SoftwareSerial.h` - Native include

## General Notes

The button is a bit slow, the 2 flow metres consume all of the interrupts on an Arduino Uno, so the button is done with polling. Slow press is best, with updates to the display taking up to 3 seconds to reflect (Screen refresh time is 3 seconds).

![Prototype Unit](images/SprayController_prototypeUnit_v1.0.jpg)
_Figure 2: Prototype Unit._

From Fig2 you can see a sample display - 3 satellites is not enough for reliable speed and direction, you should wait until you have 6 or 7. and Due to the litres/ha being the result of dividing by speed, when speed == 0, you end up with a NaN result. It was just as meaningful as anything else you could print there at 0 speed, so was just left as was.

Powered off USB for this example, power, along with the inbound flow signals come in from the black socket on the left, and the button can be just seen on the right under the USB plug.

The GPS antenna is the basic ceramic one that comes with the NEO-6M module, and is just hot-glued to the top of the unit on the outside (Signal was far less reliable when mounted inside the plastic container.).

Hot glue is your friend when assembling something like this, as you can see, even the screen is hot glued to the container.
